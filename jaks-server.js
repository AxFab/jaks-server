/*
        Jaks - Framework Javascript
    Copyright (C) 2013  Fabien Bavent<fabien.bavent@gmail.com>

  This application is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Jaks.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Module dependencies. */
var http = require('http'),
  path = require('path'),
  tty = require('tty'),
  fs = require('fs');

/* Pre-defined console colors. */
var darkred = '\033[31m',
  red = '\033[91m', 
  blue = '\033[34m',
  purple = '\033[35m',
  aqua = '\033[36m',
  cyan = '\033[96m',
  dkgreen = '\033[32m',
  green = '\033[92m',
  dkgray = '\033[90m',
  yellow = '\033[33m',
  yellow2 = '\033[93m',
  magenta = '\033[95m',
  reset = '\033[0m';

/* Extend string prototype */
String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};
String.prototype.ltrim=function(){return this.replace(/^\s+/,'');};
String.prototype.rtrim=function(){return this.replace(/\s+$/,'');};
String.prototype.fulltrim=function(){return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');};
String.prototype.startwith=function(str){ return this.substring(0, str.length) == str;};
String.prototype.endswith=function(str){ return this.substring(this.length - str.length, this.length) == str;};

/* Define environment constant */
var isTty = function () {
  return tty.isatty(process.stdout);
}

function Server () 
{
  var prv = {
    caching : new Caching ()
  }

  var dumpError = function (err) {
    if (typeof err === 'object') {
      if (err.message) {
        console.error('\nMessage: ' + err.message)
      }
      if (err.stack) {
        console.error('\nStacktrace:')
        console.error('====================')
        console.error(err.stack);
      }
    } else {
      console.error('Error : ' + err);
    }
  }

  this.download = function (uri, req, res, qry)
  {
    res.writeHead(200, {"Content-Type": (qry.query.mimetype != null ? qry.query.mimetype : "application/octet-stream"), 
      "Content-Disposition": "attachment; filename=\""+ qry.query.filename +"\""});
    if (typeof (qry.query.content) == 'string')
      res.write(qry.query.content, "binary");  
    res.end();
    
    if ((isTty())) 
      msg = dkgray + 'DOWNLOAD ' + qry.query.filename + green + ' ' + 200 + ' ' + reset;
    else 
      msg = 'DOWNLOAD ' + qry.query.filename + ' ' + 200 + ' ';
    console.log (msg);
  }
  
  this.get = function (uri, req, res, qry)
  {
    var sess = new Session();
    sess.query = qry.query;
    if (uri.endswith ('/'))
      uri += 'index.html';
    prv.caching.reach (uri, { 
      start:new Date().getTime(),
      session:sess
    }, function (usr, data) {
      usr.end = new Date().getTime(),
      usr.elapsed = usr.end - usr.start;
      var msg = ((isTty()) ?  dkgray + 'GET ' : 'GET ') + uri;
      
      if (usr.status == 'OK') {
        // 200 - OK
        msg += ((isTty()) ? green + ' ' + 200 + ' ' + reset : ' ' + 200 + ' ' );
        res.writeHead(200, {
          "Content-Type": (usr.mimetype != null ? usr.mimetype : "application/octet-stream"), 
        });  
        res.write(data, "binary");

        res.end();
        msg += ((isTty()) ? dkgray + usr.elapsed + 'ms' + reset : usr.elapsed + 'ms');
        console.log (msg);

        // 300 - Cyan !
      } else if (usr.status == 'notfound') {
        // 404 - NOT FOUND
        prv.caching.reach ('/404.html', usr, function (usr, data) {
          msg += ((isTty()) ? yellow + ' ' + 404 + ' ' + reset : ' ' + 404 + ' ' );
          res.writeHead(404, {
            "Content-Type": (usr.mimetype != null ? usr.mimetype : "application/octet-stream"), 
          });  
          if (typeof (data) == 'string')
            res.write(data, "binary");
          res.end();
          msg += ((isTty()) ? dkgray + usr.elapsed + 'ms' + reset : usr.elapsed + 'ms');
          console.log (msg);
        });

      } else { // if (usr.status == 'error') {
        // 500 - INTERNAL ERROR
        prv.caching.reach ('/500.html', usr, function (usr, data) {
          msg += ((isTty()) ? red + ' ' + 500 + ' ' + reset : ' ' + 500 + ' ' );
          res.writeHead(500, {
            "Content-Type": (usr.mimetype != null ? usr.mimetype : "application/octet-stream"), 
          });  
          if (typeof (data) == 'string')
            res.write(data, "binary");
          res.end();
          msg += ((isTty()) ? dkgray + usr.elapsed + 'ms' + reset : usr.elapsed + 'ms');
          console.log (msg);
        });

      }
    });

  }
  
  this.listener = function (req, res) 
  {
    var qry = require('url').parse(req.url, true);
    var uri = qry.pathname;
    
    console.log (' :: ' + uri);
    
    try {

      if (uri.startwith ('/.')) {
        uri = uri.substring(2)
        if (typeof (this[uri]) === 'function') 
          this[uri] (uri, req, res, qry);
      } else 
          that.get (uri, req, res, qry);

    } catch(err) {

      if ((isTty())) 
        msg = red + 'Err. Fatal error for \'' + aqua + uri + '\'' + reset;
      else
        msg = 'Err. Fatal error for \'' + uri + '\'';
      console.error (msg);
      dumpError(err);
    }

  }
  
  this.start = function () 
  {    
    var port  = process.env.PORT || 8080;
    http.createServer(srv.listener).listen(port, function() {
      console.log('Server listening on port ' + port);
    });
  }
  
  var that = this;
  {
  }
}

function Promise (number, array) {

  var prv = {
    done:null,
    number:number, 
    intialize:number, 
    array:array
  }

  this.done = function (callback) {
    // console.log ('PROMISE', prv)
    prv.done = callback;
    if (prv.intialize == 0)
      callback ()
  }

  this.callback = function (idx, arg) {
    return function  () {
      // console.log ('MISS', idx, arg, arguments)
      prv.number -= 1;
      prv.array[idx].res = arguments[arg];
      if (prv.number == 0) {
        if (prv.done)
          prv.done ();
      }
    }
  }
  
  var that = this;
  {

  }
}

function Caching ()
{

  var cache = {};
  
  var reachResult = function (resx, usr, callback) {

    var data = resx.data;
    var err = 'OK'
    if (typeof (data) != 'string') {

      var miss = []
      for (var i=0; i<resx.data.items.length; ++i) {
        if (typeof (resx.data.items[i]) == 'string') 
          continue;
        miss.push (resx.data.items[i]);
      }
 
      var async = new Promise (miss.length, miss);

      for (var i=0; i<miss.length; ++i) {
        that.reach (miss[i].uri, usr, async.callback(i, 1));
      }

      async.done (function () {


        data = '';
        for (var i=0; i<resx.data.items.length; ++i) {
          if (typeof (resx.data.items[i]) == 'string') 
            data += resx.data.items[i];
          else 
            data += resx.data.items[i].res;
        }

        if (resx.data.parent) {
          that.reach (resx.data.parent, usr, callback, data);
          return;
        }

        if (callback) {
          usr.data = data;
          usr.status = err;
          usr.mimetype = resx.mimetype
          callback (usr, data);
        }
      });

      return;
    }

    if (callback) {
      usr.data = data;
      usr.status = err;
      usr.mimetype = resx.mimetype
      callback (usr, data);
    }
  }

  this.saveResource = function (uri, data, parent, mimetype) {
    cache[uri] = {
      data:data, 
      parent:parent,
      mimetype:mimetype
    }
  }
  
  this.reach = function (uri, usr, callback, child) {
    
    var crsx = cache[uri];
    if (crsx != null) {
      reachResult (crsx, usr, callback);
      return;
    }
    
    var ctx = new Session();
    usr.session.system = {
      parentLayout:null,
      childLayout:child
    };

    // One Caching config shoud instantiate resource, this function need to move
    new Resource (uri, usr.session, function (resx) {
    
      if (usr.session.caching == 'static' && false) // TODO add config file
        cache[uri] = resx;

      reachResult (resx, usr, callback);

    }, function (err) {
      // failure
      if (callback) {
        usr.data = null;
        usr.status = err;
        callback (usr, null);
      }
    });
  }
  
  this.get = function (uri, callback) {
    var start = new Date().getTime();
    fs.exists(uri, function(exists) {
      if(!exists) {
        callback(404, '', { request:uri, elapsed:new Date().getTime() - start });
      }
      
      var res = ccc.reach (uri, function (data, e) {
        callback (200, data, { request:uri, elapsed:new Date().getTime() - start });
      });

    });
  }
  
  var that = this;
  {
  }
}

function Markup (text, index) 
{

  this.value = ''
  this.command = ''
  this.args = []
  
  
  this.extract = function () {
    this.value = '#{';
    index += this.value.length;
    while (this.value[this.value.length-1] != '}' || 
        this.value[this.value.length-2] != '/' ||
        index >= text.length) {
      this.value += text[index++];
    }
  }
  
  this.split = function () {
  
    tag = this.value.substring(2);
    var c = '';
    
    while (tag[0] <= ' ')
    tag = tag.substring(1);
    while (tag[0] > ' ') {
      c += tag.substring(0, 1);
      tag = tag.substring(1);
    }
    
    if (c == '/}') {
      if (tty.isatty(process.stdout))
        console.log(yellow2 + 'Warning: Empty tag' + reset)
      else
        console.log('Warning: Empty tag')
    }
    
    this.command = c;
    c = '';
    for (;;) {
      while (tag[0] <= ' ')
      tag = tag.substring(1);
      while (tag[0] != ':' && (tag[0] != '/' || tag[1] != '}')) { 
        c += tag.substring(0, 1);
        tag = tag.substring(1);
      }
      
      if (c == '/}' || c == '}') {
        console.log('Cmd: '+ this.args);
        return;
      }
      
      c = c.trim();
      this.args.push(c);
      c = '';
      
      if (tag[0] == '/' && tag[1] == '}')
        return;
      tag = tag.substring(1);
    }
  }
  
  {
    this.extract ()
    this.split ()
  }
}

function Session ()
{
  // object scope
  this.current = {};
  this.query = {};
  this.session = {};
  this.il8n = {};
  this.system = {};
  
  // caching capability
  this.caching = 'static';
  this.sessionUpdate = false;
  
  /**
   * Get the value of a variable
   * @param {String} [name] variable name
   * @return {String} Value store on the variable
   * @todo Add warning variable is undefined!
   */   
  this.varGet = function (name) {
    if (name.startwith('query.')) {
      if (this.caching == 'static')
        this.caching = 'query';
      return this.query[name.substring(6, name.length)];
    }
    if (name.startwith('session.')) {
      if (this.caching != 'volatile')
        this.caching = 'session';
      return this.session[name.substring(8, name.length)];
    }
    if (name.startwith('il8n.')) 
      return this.il8n[name.substring(5, name.length)];
    if (name.startwith('system.')) {
      this.caching = 'volatile';
      return this.system[name.substring(7, name.length)];
    }
    if (name.startwith('current.')) 
      return this.current[name.substring(8, name.length)];
    return this.current[name];
  };
  
  /**
   * Assign a variable
   * @param {String} [name] variable name
   * @param {String} [value] value to store on variable
   */   
  this.varSet = function (name, value) {
    if (name.startwith('session.')) {
      this.sessionUpdate = true;
      this.session[name.substring(8, name.length)] = value;
    }
    if (name.startwith('current.')) 
      this.current[name.substring(8, name.length)] = value;
    if (name.startwith('system.') ||
      name.startwith('query.') ||
      name.startwith('il8n.'))
      throw new Error("Variable " + name + " is read only");
    return this.current[name] = value;
  };
  
  this.setParent = function (uri) {
    this.system.parentLayout = uri;
  }

  function dummyTextEval (value) 
  {
    var tokens = []
    var str = '';
    var lg = 0
    while (lg < value.length) {
      if (value[lg] == '\'') {
        while (value[++lg] != '\'' && lg < value.length)
          str += value[lg]
          tokens.push (str);
      } else {
        while (value[lg+1] != ' ' && lg < value.length)
          str += value[lg++]
        stre = that.varGet (str)
        if (stre == null) {          
          if ((isTty())) 
            msg = magenta + 'Warn. ' + reset + 'the variable \'' + aqua + str + reset + '\' is undefined'
          else 
            msg = 'Warn. the variable \'' + str + '\' is undefined'
          console.warn (msg);
        }
        tokens.push (stre != null ? stre : '');
      }
      
      lg++;
    }
    // console.log ('token', tokens.toString())
    
    if (tokens.length == 1)
      return tokens[0];
    return value;
  }
  
  this.eval = function (string) {
    return dummyTextEval (string);
  }
  
  var that = this;
  {
  }
}

function MarkupParser (cnt, ctx) 
{

  this.set = function (ctx, args) {
    if (args.length != 2) {
      if ((isTty())) 
        msg = red + 'Err. ' + reset + 'the command \'' + aqua + 'set' + reset + '\' take two arguments';
      else
        msg = 'Err. the command \'set\' take two arguments';
        
      console.error (msg);
      return '';
    }
    var value = ctx.eval (args[1]);
    
    // console.log ('set variable ' + args[0] + ' to "' + value + '"');
    ctx.varSet (args[0], value);
    
    
    return '';
  }
  
  this.get = function (ctx, args) {
    if (args.length != 1) {
      if ((isTty())) 
        msg = red + 'Err. ' + reset + 'the command \'' + aqua + 'get' + reset + '\' take one argument';
      else
        msg = 'Err. the command \'get\' take one argument';
        
      console.error (msg);
      return '';
    }
    
    return ctx.eval (args[0]);
  }
  
  this.extends = function (ctx, args) {
    if (args.length != 1) {
      if ((isTty())) 
        msg = red + 'Err. ' + reset + 'the command \'' + aqua + 'extends' + reset + '\' take one argument';
      else
        msg = 'Err. the command \'extends\' take one argument';
        
      console.error (msg);
      return '';
    }
    
    var value = ctx.eval (args[0]);
    // ctx.setParent (value);
    return { parent: value };
  }
  
  this.include = function (ctx, args) {

    if (args.length != 1) {
      if ((isTty())) 
        msg = red + 'Err. ' + reset + 'the command \'' + aqua + 'get' + reset + '\' take one argument';
      else
        msg = 'Err. the command \'get\' take one argument';
        
      console.error (msg);
      return '';
    }
    
    var value = ctx.eval (args[0]);
    return { uri:value };
  }

  this.doLayout = function (ctx, args) {
    var xn = ctx.varGet ('system.childLayout');
    return xn != null ? xn : '';
  }

  {
    parent = null;
    items = []
    while ((k = cnt.search('#{')) >= 0) {
      var markup = new Markup (cnt, k);
      if (typeof (this[markup.command]) === 'function') {
        var xn = this[markup.command] (ctx, markup.args);
        if (typeof (xn) == 'string')
          cnt = cnt.replace (markup.value, xn);
        else {
          items.push (cnt.substring (0, k));
          if (xn.parent != null)
            parent = xn.parent;
          else 
            items.push (xn)
          cnt = cnt.substring(k + markup.value.length);
        }
        // console.log ('Markup found', markup.value, xn, cnt, items);
      } else {
        if ((isTty())) 
          msg = magenta + 'Warn. ' + reset + 'the command \'' + aqua + markup.command + reset + '\' is not available'
        else 
          msg = 'Warn. the command \'' + markup.command + '\' is not available'
        console.warn (msg);
        
        cnt = cnt.replace (markup.value, '');
      }
    }

    items.push (cnt.trim());


    if (items.length == 1 && parent == null)
      return items[0];
    else {
      return { items:items, parent:parent }
    }
  }
}

function Resource (uri, ctx, callback, failure) 
{  
  var prv = {
    status:'loading',
    content:null,
  }

  this.uri = uri;
  this.mimetype = '';
  this.data = '';
  
  var getMimetype = function (uri) {

    // TODO Load this from config file
    var mimeExt = [
      { ext:'.html', mtype:'text/html' },
      { ext:'.css', mtype:'text/css' },
      { ext:'.csv', mtype:'text/csv' },
      { ext:'.txt', mtype:'text/plain' },
      { ext:'.md', mtype:'text/plain' },
      { ext:'.wk', mtype:'text/plain' },

      { ext:'.gif', mtype:'image/gif' },
      { ext:'.jpg', mtype:'image/jpg' },
      { ext:'.jpeg', mtype:'image/jpg' },
      { ext:'.png', mtype:'image/png' },
      { ext:'.tiff', mtype:'image/tiff' },

      { ext:'.json', mtype:'application/json' },
      { ext:'.js', mtype:'application/javascript' },
      { ext:'.xml', mtype:'application/xml' },
      { ext:'.pdf', mtype:'application/pdf' },
      { ext:'.zip', mtype:'application/zip' },
    ];

    for (var i=0; i<mimeExt.length; ++i) {
      if (uri.endswith(mimeExt[i].ext))
        return mimeExt[i].mtype;
    }
    return 'application/octet-stream'
  }

  this.load = function (callback) {
    uri = path.join(__dirname, uri);

    fs.exists(uri, function(exists) {
      if(!exists) {  
        prv.status = 'notfound'
        if (failure)
          failure (prv.status);
      } else {

        fs.readFile(uri, "binary", function(err, file) {  
          if(err) {  
            if (err.message == 'EISDIR, read') {
              prv.status = 'directory'
              if (failure)
                failure (prv.status);
            } else {
              prv.status = 'error'
              if (failure)
                failure (prv.status);
            }
          } else {
            prv.status = 'loaded'
            prv.content = file
            
            // TODO Extention base config
            if (uri.endswith ('.html')) {
              prv.content = MarkupParser (prv.content, ctx)
            }
            
            // Add it on the ressource config !
            that.data = prv.content;
            if (callback)
              callback (that, prv.content);
          }
        });
      }
    });
  }
  
  var that = this;
  {
    this.mimetype = getMimetype(uri);
    this.load(callback);
  }
}

/* 
// Single file debugging code

var sess = new Session();
var ccc = new Caching ();

ccc.reach ('index.html', { 
  start:new Date().getTime(),
  session:sess
}, function (usr, data) {
  if (usr.status == 'OK')
    console.log (data);
  else if (usr.status == 'notfound')
    console.log ('404');
  else if (usr.status == 'error')
    console.log ('500');
});
*/

// Server main --------------------------------------------------------------
if (require.main === module) {
  var srv = new Server ();
  srv.start ();
}

